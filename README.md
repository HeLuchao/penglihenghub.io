[![Build Status](https://travis-ci.org/pengliheng/pengliheng.github.io.svg?branch=master)](https://travis-ci.org/pengliheng/pengliheng.github.io)
[![author](https://img.shields.io/badge/author-peng-blue.svg)](https://github.com/pengliheng/pengliheng.github.io)
[![Node.js Version](https://img.shields.io/badge/node.js-8.7.0-blue.svg)](http://nodejs.org/download)
![Size](https://github-size-badge.herokuapp.com/pengliheng/pengliheng.github.io.svg)


### 欢迎光临我的博客

###### 本站托管于Github，所有数据来源于Github，通过动态加载数据渲染页面

### My English Introduction
Hello everyone, my name is Joy and now i'm going to introducte myself. i was born in south china, my birthday is Sep.6 ,and now i am 26th years old.I got my bachelor's degree in South China University of Technology, Guangzhou College
, major in civil Engineering, but now i am an web developer. my recently aims is join in tencent. best wish to me. thank u!



### TODO
- [ ] An Overview of Arrays and Memory - 文章
- [ ] 代码片段Gist
- [ ] serverless
- [ ] 任意写一个Vscode插件
- [ ] 参考[less-watch-compile](https://github.com/pawlh/less-watch-compile)+[postcss](https://github.com/postcss/postcss)写一个拥有自动转换less->css+[autoprefixer](https://github.com/postcss/autoprefixer) - 库
- [ ] 静态类型检查 - typescript


### 发展方向
- Graphql API
- React + next.js 从服务器端渲染页面
- Github Issue Page - server less
- GoLang 搭建后端
- ~~Vue搭建前端页面~~
- 代码片段Gist
- 分析Github相关数据


### 特性
- [x] 博客
- [x] 仓库集合
- [x] 计划任务TODO
- [x] 响应式
- [x] 数据持久化
- [x] 按需加载
- [ ] 静态类型检查

### 技术栈
- React
- Graphql
- Ant-Design
- Github Api

### 贡献代码

```bash
git clone https://github.com/pengliheng/pengliheng.github.io.git
cd ./blog
yarn            # 安装依赖
yarn run w      # 监听10086端口
yarn run nodemon      # 监听10086端口
```
**Welcome PR 😀**
